# Docker-BaiduPan

## Introduction

This is a useful tool to synchronize your data between Baidu Pan and any environment that supports Docker. It uses [Alpine Linux](https://hub.docker.com/_/alpine/) as base layer and [bypy](https://github.com/houtianze/bypy) as the client.



## Quick Start

#### Step 1

```
docker pull kukki/docker-baidupan
```

#### Step 2

```
docker run -it --restart on-failure --name docker-baidupan -v /path/to/bypy:/bypy kukki/docker-baidupan
```

*Note:*

* Update `/path/to` to your actual path you want to sync with Baidu Pan
* `--restart on-failure` is to set auto-restart when expected error occurs

#### Step 3

1. Copy the URL in the prompt and visit it on your browser.
2. It authenticates and authorizes your Baidu account
3. After you accept the application, a string code is ready for you.
4. Copy the code, paste it to the prompt and hit `Enter`

#### Step 4

You can keep the docker container running in the foreground

-- or --

You can hit `CTRL-C` to stop it, and then type the following command to restart the container.

```
docker start docker-baidupan
```

## Limitation

* Because of Baidu API limitation, the container can upload files and folders to the remote folder /app/bypy in Baidu Pan.
* The pre-built image support one-direction sync only, the default direction is from local to remote. You can follow the steps to change the direction.

#### Change synchronize direction

```
git clone git@bitbucket.org:jkoo/docker-baidupan.git
# syncup (from local to remote)
# syncdown (from remote to local)
sed -i 's/syncup/syncdown/g' bypy-daemon.sh
docker build -t docker-baidupan .
docker run -it --restart on-failure --name docker-baidupan -v /path/to/bypy:/bypy docker-baidupan
```

## Reference

* [houtianze/bypy](https://github.com/houtianze/bypy) (by Hou Tianze)

## License

* MIT