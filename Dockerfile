FROM ubuntu:18.04

MAINTAINER Lee JongYoung<leejongyoung@icloud.com>

RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update && \
    apt-get install -y locales aria2 git make build-essential libssl-dev \
    zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
    libncurses5-dev libncursesw5-dev xz-utils libffi-dev liblzma-dev ca-certificates \
    && rm -rf /var/lib/apt/lists/* \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN git clone git://github.com/pyenv/pyenv.git .pyenv
RUN git clone https://github.com/pyenv/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv

ENV HOME  /
ENV PYENV_ROOT $HOME/.pyenv
ENV PATH $PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH

RUN pyenv install 3.6.6
RUN pyenv global 3.6.6
RUN pyenv rehash

RUN pip install --upgrade pip
RUN pip install requests
RUN pip install bypy
RUN mkdir /bypy

VOLUME ["/bypy"]

COPY bypy-daemon.sh /
CMD /bypy-daemon.sh